Module {
    name: 'vault'
    author: 'Dean Powell <PowellDean@gmail.com>'
    description: 'V wrapper for V Hashicorp's Vault API'
    version: '1.0.0'
    license: 'APLv2'
    vcs: 'git'
    tags: ['api', 'vault', 'vlang']
    dependencies: []
}
