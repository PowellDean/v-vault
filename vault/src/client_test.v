module main

import vault

fn test_new_client_no_args() {
	assert vault.new_client().to_string() == 'http://127.0.0.1:8200'
}
